﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture8
{
    public static class ArrayExtensions
    {
        public static string ArrayToString(this int[] array, string separator)
        {
            var result = String.Empty;

            foreach (var i in array)
            {
                result += i.ToString() + separator + " ";
            }

            return result.Remove(result.Length - 2);
        }

        static void Main()
        {
            var array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

            Console.WriteLine(ArrayExtensions.ArrayToString(array, " SEPARATOR"));
            Console.WriteLine("------------------------");
            Console.WriteLine(array.ArrayToString("---"));
            Console.ReadKey();
        }
    }
}
